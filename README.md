# MLIR Transform Dialect Tester

The MLIR [Transform](https://mlir.llvm.org/docs/Dialects/Transform/) dialect provides operations that can be used to control transformation of the IR using a different portion of the IR. It refers to the IR being transformed as payload IR, and to the IR guiding the transformation as transform IR. A [tutorial](https://mlir.llvm.org/docs/Tutorials/transform/) is also available in the MLIR website.

The transform dialect lends itself quite well to structured operations (`linalg`) for more advanced algorithm implementations using common optimizations such as tiling, packing, vectorization, etc. The objective of this project is to implement the SConv convolution algorithm (or something similar) as transform IR.

This repository houses scripts to test transform IR files on a payload, and some examples. 

## Prerequisites
Build the LLVM Project with the MLIR project enabled. Here is an example using CMake and Ninja:
``` bash
mkdir llvm-project/build
cd llvm-project/build
cmake -G Ninja ../llvm \
   -DLLVM_ENABLE_PROJECTS=mlir \
   -DLLVM_TARGETS_TO_BUILD="host" \
   -DCMAKE_BUILD_TYPE=Debug \
   -DLLVM_ENABLE_ASSERTIONS=ON \
   -DLLVM_ENABLE_RTTI=ON

cmake --build . -- ${MAKEFLAGS}
cmake --build . --target check-mlir
```

Set the `LLVM_BIN_PATH` environment variable to the full path to the folder containing the binary executables from LLVM, likely `llvm-project/build/bin`.

## Testing
To test transform IR files on a payload IR file, use the `opt.sh` script. This will not apply any optimization or lowering passes to the payload IR aside from the transformation sequence.

To finish lowering the payload IR and execute it (for correctness testing), use `run.sh`. This script creates a `.llvm.mlir` file with the final LLVM dialect IR, and JIT compiles it using `mlir-cpu-runner`. The entry point will be the `main` function.

The `opt_and_run.sh` file combines both scripts into one, applying the transformation sequence and then lowering and executing the payload IR, as described above.

The `test_transform.sh` script compares the output of `opt_and_run.sh` with a transformation sequence with the output of `run.sh` with the direct lowering of the payload IR to check for correctness. It prints `SUCCESS` if the outputs are the same, and `FAILURE` otherwise (discarding the first line of each output).

OBS: `run.sh` does not necessarily perform a full lowering of the payload IR, as it only uses a few specific lowering passes. For any more robust lowering needed, editing the script will be necessary. It does perform bufferization, however.

## Payloads
The payloads in this repository are MLIR files with structured operations and a `main` function.

- `conv.mlir`: a simple large convolution to test SConv-like transformations. Its `main` function includes performance measurement code and a simple driver to run the convolution multiple times.

## Examples
The examples in this repository are snippets of transformation sequences over the `linalg.conv_2d_nchw_fchw` structured operation (`conv.mlir`). 

- `sconv`: SConv-like transformations
- `1d-conv`: IREE convolution lowering path through 1D decomposition

Some of the transformations in `sconv` are:

- `tiling.mlir`: simple convolution tiling based on common CSA values for that convolution shape.
- `highlevel.mlir`: transform the tiled convolution into a partial im2col and a matmul, and apply some opt. patterns.
- `bufferization.mlir`: apply bufferization to start lowering the convolution

`full.mlir` is the most complete transformation sequence at this point, for each directory.

## On SConv
SConv is a novel direct convolution algorithm developed to leverage SIMD architectures and ISA extensions. For more information, refer to:

Ferrari, V., Sousa, R., Pereira, M., de Carvalho, J. P., Amaral, J. N., Moreira, J., & Araujo, G. (2023). Advancing Direct Convolution using Convolution Slicing Optimization and ISA Extensions. arXiv preprint arXiv:2303.04739.

[Link](https://arxiv.org/abs/2303.04739)