#!/bin/bash

# Verify if both $1 and $2 are set
if [ -z "$1" ] || [ -z "$2" ]; then
    echo "Usage: $0 <base_mlir_file> <csv_file>"
    echo "  <base_mlir_file>: Path to the base MLIR template file."
    echo "  <csv_file>: Path to the CSV file containing convolution configurations."
    exit 1
fi

# Paths
BASE_MLIR_FILE=$1  			# Base template MLIR file
CSV_FILE=$2                            	# CSV file with convolution parameters
OUTPUT_DIR="generated_mlir_files_lucas"       # Output directory

# Create output directory if it doesn't exist
mkdir -p $OUTPUT_DIR

IFS=, read -r -a HEADER < "$CSV_FILE"

# Read CSV file and skip the header
tail -n +2 "$CSV_FILE" | while IFS=, read -r "${HEADER[@]}"; do
    PHI=$((HI + HPBOTTOM + HPTOP))
    PWI=$((WI + WPLEFT + WPRIGHT))

    FLOPS=$((2 * HO * WO * DO * CI * HK * WK))
    
    # Check the condition before generating the file
    if (( (HO * WO) % 16 == 0 && DO % 8 == 0 )); then
        # Generate the output file name
        OUTPUT_FILE="${OUTPUT_DIR}/conv_${ID}.mlir"

        # Copy the base template and replace placeholders using `sed`
        sed \
            -e "s/{{ID}}/${ID}/g" \
            -e "s/{{NI}}/${NI}/g" \
            -e "s/{{CI}}/${CI}/g" \
            -e "s/{{HI}}/${HI}/g" \
            -e "s/{{PHI}}/${PHI}/g" \
            -e "s/{{WI}}/${WI}/g" \
            -e "s/{{PWI}}/${PWI}/g" \
            -e "s/{{NO}}/${NO}/g" \
            -e "s/{{DO}}/${DO}/g" \
            -e "s/{{HO}}/${HO}/g" \
            -e "s/{{WO}}/${WO}/g" \
            -e "s/{{HK}}/${HK}/g" \
            -e "s/{{WK}}/${WK}/g" \
            -e "s/{{HPTOP}}/${HPTOP}/g" \
            -e "s/{{HPBOTTOM}}/${HPBOTTOM}/g" \
            -e "s/{{WPLEFT}}/${WPLEFT}/g" \
            -e "s/{{WPRIGHT}}/${WPRIGHT}/g" \
            -e "s/{{HS}}/${HS}/g" \
            -e "s/{{WS}}/${WS}/g" \
            -e "s/{{HD}}/${HD}/g" \
            -e "s/{{WD}}/${WD}/g" \
            -e "s/{{GROUP}}/${GROUP}/g" \
            -e "s/{{BIAS}}/${BIAS}/g" \
            -e "s/{{FLOPS}}/${FLOPS}/g" \
            "$BASE_MLIR_FILE" > "$OUTPUT_FILE"    

        echo "Generated: $OUTPUT_FILE"
    else
        echo "Skiped conv $ID"
    fi
done

