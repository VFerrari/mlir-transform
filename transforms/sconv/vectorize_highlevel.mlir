module attributes {transform.with_named_sequence} {
   transform.named_sequence @__transform_main(
     %arg0: !transform.any_op) {
    %conv = transform.structured.match ops{["linalg.conv_2d_nchw_fchw"]} in %arg0
      : (!transform.any_op) -> !transform.op<"linalg.conv_2d_nchw_fchw">

    // Step 1: TILING
    // ======================================================
    %conv_l2, %loops_l2:5 = transform.structured.tile_using_for %conv
      // N, F, OH, OW, C, KH, KW
      tile_sizes [1, 64, 1, 32, 16, 0, 0]
      interchange = [0, 4, 3, 2, 1] 
      : (!transform.op<"linalg.conv_2d_nchw_fchw">)
      -> (!transform.op<"linalg.conv_2d_nchw_fchw">, !transform.any_op, 
          !transform.any_op, !transform.any_op, !transform.any_op, 
          !transform.any_op)

    %conv_l3, %loops_l3:2 = transform.structured.tile_using_for %conv_l2
      // N, F, OH, OW, C, KH, KW
      tile_sizes [0, 8, 0, 16, 0, 0, 0]
      interchange = [1, 0]
      : (!transform.op<"linalg.conv_2d_nchw_fchw">)
     -> (!transform.op<"linalg.conv_2d_nchw_fchw">, !transform.any_op,
          !transform.any_op)


    // Step 2: Im2Col + Matmul
    // ======================================================
    %img2col, %matmul = transform.structured.convert_conv2d_to_img2col %conv_l3
      : (!transform.op<"linalg.conv_2d_nchw_fchw">) 
      -> (!transform.any_op, !transform.any_op)


    // Optimize!
    // Get container and apply first opt. patterns
    %container = transform.structured.match ops{["func.func"]} in %arg0 
      : (!transform.any_op) -> !transform.any_op

    transform.apply_patterns to %container {
      transform.apply_patterns.linalg.tiling_canonicalization
      transform.apply_patterns.tensor.merge_consecutive_insert_extract_slice
    } : !transform.any_op

    // Step 3: Vectorize MatMul
    // ======================================================
    %vectorized = transform.structured.vectorize_children_and_apply_patterns %container
      : (!transform.any_op) -> !transform.any_op
    
    transform.yield
  }
}