module attributes {transform.with_named_sequence} {
   transform.named_sequence @__transform_main(
     %arg0: !transform.any_op) {
    %conv = transform.structured.match ops{["linalg.conv_2d_nchw_fchw"]} in %arg0
      : (!transform.any_op) -> !transform.op<"linalg.conv_2d_nchw_fchw">


    // Step 1: TILING
    // ======================================================
    %conv_l2, %loops_l2:5 = transform.structured.tile_using_for %conv
      // N, F, OH, OW, C, KH, KW
      tile_sizes [1, 64, 1, 32, 16, 0, 0]
      interchange = [0, 4, 3, 2, 1] 
      : (!transform.op<"linalg.conv_2d_nchw_fchw">)
      -> (!transform.op<"linalg.conv_2d_nchw_fchw">, !transform.any_op, 
          !transform.any_op, !transform.any_op, !transform.any_op, 
          !transform.any_op)

    %conv_l3, %loops_l3:2 = transform.structured.tile_using_for %conv_l2
      // N, F, OH, OW, C, KH, KW
      tile_sizes [0, 8, 0, 16, 0, 0, 0]
      interchange = [1, 0]
      : (!transform.op<"linalg.conv_2d_nchw_fchw">)
     -> (!transform.op<"linalg.conv_2d_nchw_fchw">, !transform.any_op,
          !transform.any_op)


    // Step 2: Im2Col + Matmul
    // ======================================================
    %img2col, %matmul = transform.structured.convert_conv2d_to_img2col %conv_l3
      : (!transform.op<"linalg.conv_2d_nchw_fchw">) 
      -> (!transform.any_op, !transform.any_op)


    // Optimize!
    // Get container and apply first opt. patterns
    %container = transform.structured.match ops{["func.func"]} in %arg0 
      : (!transform.any_op) -> !transform.any_op

    transform.apply_patterns to %container {
      transform.apply_patterns.linalg.tiling_canonicalization
      transform.apply_patterns.tensor.merge_consecutive_insert_extract_slice
    } : !transform.any_op

    // Step 3: Vectorize MatMul
    // ======================================================
    %vectorized_container = transform.structured.vectorize_children_and_apply_patterns %container
      : (!transform.any_op) -> !transform.any_op

    // Step 4: BUFFERIZATION
    // ======================================================
    // Eliminate empty tensors when possible
    //transform.bufferization.eliminate_empty_tensors %vectorized_container
    //  : !transform.any_op

    // Allocate empty tensors
    %empty = transform.structured.match ops{["tensor.empty"]} in %arg0 
      : (!transform.any_op) -> !transform.any_op
    %empty_op = transform.cast %empty 
      : !transform.any_op to !transform.op<"tensor.empty">
    %alloc = transform.bufferization.empty_tensor_to_alloc_tensor %empty_op
      : (!transform.op<"tensor.empty">) -> !transform.op<"bufferization.alloc_tensor">

    // One-Shot Bufferize
    %bufferized = transform.bufferization.one_shot_bufferize %arg0
      { bufferize_function_boundaries = true }
      : (!transform.any_op) -> !transform.any_op
  
    // Optimize!
    // TODO: best patterns
    %bufferized_container = transform.structured.match ops{["func.func"]} in %bufferized 
      : (!transform.any_op) -> !transform.any_op

    transform.apply_patterns to %bufferized_container {
      //transform.apply_patterns.memref.fold_memref_alias_ops
      transform.apply_patterns.memref.extract_address_computations 
      transform.apply_patterns.memref.expand_strided_metadata 
    } : !transform.any_op

    // Step 5: Lower to LLVM
    // ======================================================

    // Lower vector operations with patterns
    transform.apply_patterns to %bufferized_container {
      transform.apply_patterns.vector.transfer_to_scf 
        max_transfer_rank = 1 full_unroll = true
      transform.apply_patterns.vector.lower_contraction 
        lowering_strategy = "outerproduct"
      transform.apply_patterns.vector.lower_transfer 
        max_transfer_rank = 1
      transform.apply_patterns.vector.lower_transpose 
        lowering_strategy = "shuffle_1d"
      transform.apply_patterns.vector.lower_shape_cast
    } : !transform.any_op

    // Late canonicalizations and cleanups.
    transform.apply_patterns to %bufferized_container {
      transform.apply_patterns.canonicalization
    } : !transform.any_op
    transform.apply_cse to %bufferized_container : !transform.any_op
    %final = transform.apply_registered_pass "loop-invariant-code-motion" 
      to %bufferized_container : (!transform.any_op) -> !transform.any_op
      
    transform.yield
  }
}