module attributes {transform.with_named_sequence} {
   transform.named_sequence @__transform_main(
     %arg0: !transform.any_op) {
    %conv = transform.structured.match ops{["linalg.conv_2d_nchw_fchw"]} in %arg0
      : (!transform.any_op) -> !transform.op<"linalg.conv_2d_nchw_fchw">

    // Step 1. Tile to sequential scf.for. 
    // First level with some interchange and second level with sizes if 1 to 
    // properly target decomposition and vectorization.
    // ======================================================
    %conv_l2, %loops_l2:4 = transform.structured.tile_using_for %conv
    // N,  F, OH, OW,  C, KH, KW
      tile_sizes [0,  64,  4,  32,  16,  0,  0]
      interchange = [1, 0, 4, 2, 3]
      : (!transform.op<"linalg.conv_2d_nchw_fchw">)
      -> (!transform.op<"linalg.conv_2d_nchw_fchw">, !transform.any_op, 
          !transform.any_op, !transform.any_op, !transform.any_op)

    // Decompose needs both OH/KH or OW/KW to be tiled to 1.
    // This is required to further enable vectorization.
    %conv_l3, %loops_l3:4 = transform.structured.tile_using_for %conv_l2
    // N,  F, OH, OW,  C, KH, KW  
      tile_sizes [0,  8,  1,  12,  0,  1,  0]
      : (!transform.op<"linalg.conv_2d_nchw_fchw">)
      -> (!transform.op<"linalg.conv_2d_nchw_fchw">, !transform.any_op,
          !transform.any_op, !transform.any_op, !transform.any_op)

    // Optimize!
    // Get container and apply first opt. patterns
    %container = transform.structured.match ops{["func.func"]} in %arg0 
      : (!transform.any_op) -> !transform.any_op

    transform.apply_patterns to %container {
      transform.apply_patterns.linalg.tiling_canonicalization
    } : !transform.any_op
    
    transform.yield
  }
}