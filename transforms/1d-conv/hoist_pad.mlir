module attributes {transform.with_named_sequence} {
   transform.named_sequence @__transform_main(
     %arg0: !transform.any_op) {
    %conv = transform.structured.match ops{["linalg.conv_2d_nchw_fchw"]} in %arg0
      : (!transform.any_op) -> !transform.op<"linalg.conv_2d_nchw_fchw">

    // Step 1. Tile to sequential scf.for. 
    // First level with some interchange and second level with sizes if 1 to 
    // properly target decomposition and vectorization.
    // ======================================================
    %conv_l2, %loops_l2:4 = transform.structured.tile_using_for %conv
    // N,  F, OH, OW,  C, KH, KW
      tile_sizes [0,  64,  4,  32,  16,  0,  0]
      interchange = [1, 0, 4, 2, 3]
      : (!transform.op<"linalg.conv_2d_nchw_fchw">)
      -> (!transform.op<"linalg.conv_2d_nchw_fchw">, !transform.any_op, 
          !transform.any_op, !transform.any_op, !transform.any_op)

    // Decompose needs both OH/KH or OW/KW to be tiled to 1.
    // This is required to further enable vectorization.
    %conv_l3, %loops_l3:4 = transform.structured.tile_using_for %conv_l2
    // N,  F, OH, OW,  C, KH, KW  
      tile_sizes [0,  8,  1,  12,  0,  1,  0]
      : (!transform.op<"linalg.conv_2d_nchw_fchw">)
      -> (!transform.op<"linalg.conv_2d_nchw_fchw">, !transform.any_op,
          !transform.any_op, !transform.any_op, !transform.any_op)

    // Step 3. Force padding of all dimensions and hoist the lhs/rhs pad ops.
    // ======================================================================
    // TODO: hoisting the output requires additional support atm.
    %conv_padded_l3, %conv_pad_l3, %_ = transform.structured.pad %conv_l3 {
      padding_values = [0.0 : f32, 0.0 : f32, 0.0 : f32], 
      padding_dimensions = [0, 1, 2, 3, 4, 5, 6], 
      pack_paddings=[1, 1, 1]
    } : (!transform.op<"linalg.conv_2d_nchw_fchw">) -> (!transform.any_op, 
        !transform.any_op, !transform.any_op)

    transform.apply_patterns to %arg0 {
      transform.apply_patterns.canonicalization
    } : !transform.any_op
    transform.apply_cse to %arg0 : !transform.any_op

    %pad_input = transform.get_producer_of_operand %conv_padded_l3[0]
      : (!transform.any_op) -> !transform.op<"tensor.pad">
    transform.structured.hoist_pad %pad_input by 4 loops
      : (!transform.op<"tensor.pad">) -> !transform.any_op

    %pad_kernel = transform.get_producer_of_operand %conv_padded_l3[1]
      : (!transform.any_op) -> !transform.op<"tensor.pad">
    transform.structured.hoist_pad %pad_kernel by 3 loops
      : (!transform.op<"tensor.pad">) -> !transform.any_op
      
    %full_licm = transform.apply_registered_pass "loop-invariant-code-motion" 
      to %arg0 : (!transform.any_op) -> !transform.any_op

    transform.yield
  }
}